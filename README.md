# Laboratorio en el cliente web

**Nota**: Profesor Alberto, gracias por hacer este laboratorio un actividad divertida, completa y sencilla para aplicar muchos conceptos en el desarrollo web.

**Nota2**: El repositorio del proyecto se encuentra en <https://bitbucket.org/nayko/laboratorio_cliente_web/overview>. En la carpeta *assets/js/* se encuentra un archivo de JavaScript correspondiente a cada uno de los puntos del laboratorio.

**Nota3 (Broma)**: Un chiste de Chuck Norris sin foto es peligroso, quien sabe lo que él me pueda hacer por cometer ese sacrilegio.

##Desarrollo de la actividad

###Resolución del ejercicio a la manera de 2005 (chuck2005.html)
Cabe resaltar que una vez tuve que trabajar en un proyecto sin *JQuery*, al principio me pareció difícil, pero me fui acostumbrando. Debo decir que hasta ese momento no comprendía correctamnte que era *DOM* y como se manipulaba, yo solo me limitaba a usar el selector "$".

####La vista de la esta página se ve así:

![alt text](https://bytebucket.org/nayko/laboratorio_cliente_web/raw/eed98759ad67abb46bd71489a7283d7dd033383d/screenshots/chuck2005.png "Vista chuck2005.html")

####Mientras que el código html (chuck2005.html) es el siguiente:

```html
<!DOCTYPE html>
<html>
    <head>
        <!--mata tags-->
        <meta charset="UTF-8">
        <meta name="description" content="Hacer un chiste ChuckNorris como en 2005">
        <meta name="author" content="Nicolas Pedraza Luengas">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--Titulo de la Pagina-->
        <title>Chuck Norris en 2005</title>
        <!--Hoja de estilos de bootstrap-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
        <!--Estilos propios-->
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
    </head>

    <body>
        <!--jumbotron-->
        <div class="jumbotron joke">
                <!--Imagen de Chuck Norris, muy importante en los cihste de Chuck Norris-->
                <img class="chuckFoto" src="assets/img/chuck-norris2.jpg">
                <!--H! para el chiste-->
                <h1></h1>
                <hr class="my-4">
                <a class="btn btn-primary btn-lg" href="#" role="button">Nuevo Chiste</a>
        </div>
        <p class="susto">Pongo la foto de Chuck Norris en el chiste, quien sabe que pasara si se entera que no puse su foto</p> 
        <!--Invocacion de la logica de java script-->
        <script src="assets/js/js-2005.js"></script>
    </body>

</html>
```
####Por último, el código de Java Script (js-2005.js) es el siguiente:
```javascript
/**
* Funcion para obtener chistes
**/
function getJoke(){
	xmlhttp = new XMLHttpRequest();
	xmlhttp.open('GET', 'http://api.icndb.com/jokes/random/', true);
	xmlhttp.onreadystatechange = function(){
	    var textoChiste = JSON.parse(this.response).value.joke;
	    console.log('chiste recibido: ' + textoChiste);
	    var h1s = document.getElementsByTagName('h1');
	    h1s[0].innerHTML = textoChiste;

	}
	xmlhttp.send();
}
//Se le asiggna al boton un envento click para cambiar de chiste
var boton = document.getElementsByClassName("btn")[0];
boton.addEventListener('click', function() { 
	getJoke(); 
});
//Se llama la funcionpara que se ejecute cuando se carga la pagina
getJoke();
```
A modo de practica y organización convertí el scrpit suministrado en la guía en una función. Adicionalmente use el botón dentro de jumbotorn para hacer que el chiste fuera dinámico y cambiara cada vez que fuera oprimido.

###Resolución del ejercicio en 2006 (chuck2006.html)
Usar *Jquery* facilita mucho el trabajo, hace que el código que uno escribe sea más conciso y fácil de leer, como desventaja es que no se sabe que instrucciones están corriendo por detrás, sin contar que, en una sencilla aplicación como esta, se carga una librería con muchas funciones que al final no se terminan usando.

####La vista de la esta página se ve así:
![alt text](https://bytebucket.org/nayko/laboratorio_cliente_web/raw/eed98759ad67abb46bd71489a7283d7dd033383d/screenshots/chuck2006.png "Vista chuck2006.html")

####Mientras que el código html (chuck2006.html) es el siguiente:
```html
<!DOCTYPE html>
<html>
    <head>
        <!--mata tags-->
        <meta charset="UTF-8">
        <meta name="description" content="Hacer un chiste ChuckNorris como en 2006">
        <meta name="author" content="Nicolas Pedraza Luengas">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--Titulo de la Pagina-->
        <title>Chuck Norris en 2006</title>
        <!--Hoja de estilos y scrpist de bootstrap-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
        <!--Estilos propios-->
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
        <!--jquery-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </head>

    <body>
        <!--jumbotron-->
        <div class="jumbotron joke">
                <!--Imagen de Chuck Norris, muy importante en los cihste de Chuck Norris-->
                <img class="chuckFoto" src="assets/img/chuck-norris1.jpg">
                <!--H1 para el chiste-->
                <h1></h1>
                <hr class="my-4">
                <a class="btn btn-primary btn-lg" href="#" role="button">Nuevo Chiste</a>
        </div>
        <p class="susto">Pongo la foto de Chuck Norris en el chiste, quien sabe que pasara si se entera que no puse su foto</p>
        <hr class="my-4">
        <!--Mas etiquetas H1 para experimentar-->
        <h1>Miremos que pasa si hay mas etiquetas H1 como esta</h1>
        <h1>o como esta</h1>
        <!--Invocacion de la logica de java script-->
        <script src="assets/js/js-2006.js"></script>
    </body>

</html>
```
####Por último, el código de Java Script (js-2006.js) es el siguiente:
```javascript
/**
* Funcion para obtener chistes
**/
function getJoke(){
	$.get("http://api.icndb.com/jokes/random", (response) => {
    	var textoChiste = response.value.joke;
    	$('h1').text(textoChiste);
	})
}
//Se le asiggna al boton un envento click para cambiar de chiste
var boton = document.getElementsByClassName("btn")[0];
boton.addEventListener('click', () => { 
	getJoke(); 
});
//Se llama la funcion para que se ejecute cuando se carga la pagina
getJoke();
```
Se mantuvo la misma estructura del ejercicio anterior, solo se remplazó el contenido de la función get_joke() por el script que utiliza Jquery. 

####¿Qué pasa con más de un `H1`?
En la parte inferior del código puse un par de etiquetas `H1` para hacer esta prueba, el resultado es que el selector de *Jquery* ($) encuentra todos los elementos correspondientes y a cada uno le aplica la acción que se está justo después del punto de la selección, en este caso `text(textoChiste);`. El resultado es que todas las etiquetas `H1` dentro del *DOM* se les remplaza el texto por el valor de la variable `textoChiste`.

###Resolución con plugin de JQuery (chuck2014.html)
El uso de plugins hizo la vida más fácil, el código se redujo en gran medida y ahora solo no se debe ocupar por la lógica de nuestra aplicación

####La vista de la esta página se ve así:
![alt text](https://bytebucket.org/nayko/laboratorio_cliente_web/raw/eed98759ad67abb46bd71489a7283d7dd033383d/screenshots/chuck2014.png "Vista chuck2014.html")

####Mientras que el código html (chuck2014.html) es el siguiente:
```html
<!DOCTYPE html>
<html>
    <head>
        <!--mata tags-->
        <meta charset="UTF-8">
        <meta name="description" content="Hacer un chiste ChuckNorris como en 2014">
        <meta name="author" content="Nicolas Pedraza Luengas">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--Titulo de la Pagina-->
        <title>Chuck Norris en 2014</title>
        <!--Hoja de estilos y scrpist de bootstrap-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
        <!--Estilos propios-->
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
        <!--jquery-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!--ICMDb plugin-->
        <script src="http://code.icndb.com/jquery.icndb.min.js"></script>
    </head>

    <body>
        <h1 class="text-center"> Chuck Norreate de la risa XD</h1>
        <!--Lista de chiste de forma dinamica-->
        <div id="jokeList" class="list-group">
        </div>
        
        <!--Invocacion de la logica de java script-->
        <script src="assets/js/js-2014.js"></script>
    </body>

</html>
```
####Por último, el código de Java Script (js-2014.js) es el siguiente:
```javascript
/**
* Funcion para construir los elementos de la lista
**/
constructElement = (joke, numberJoke, foto) =>{
	var html = '<div class="list-group-item list-group-item-action flex-column align-items-start "><div class="row"><div class="col-md-3"><img class="chuckFoto" src="' + foto +'"></div><div class="col-md-9"><div class="jokeNumber"><small>Chiste número '+ numberJoke+'</small></div><h2>'+joke+'</h2></div></div></div>';
	return html
}
/**
* Funcion para obtener fotos aleatoreas
**/
fotoName = () =>{
	var number = Math.floor((Math.random() * 5) + 1);
	return 'assets/img/chuck-norris' + number + '.jpg'; 
}

//Uso del pluig solicitando 10 chistes diferentes
$.icndb.getRandomJokes({ 
    number: 10, 
    success: (response) => {
		response.forEach(element => { 
			var Name = fotoName();
			//Agregar elementos a la lista
			$("#jokeList").append(constructElement(element.joke, element.id, Name));
		});
	}
});
```
Para este caso se programó un generador de nombre de fotos aleatorios, esto con el fin de hacer la página un poco más divertida. También se hace uso de listas con estilos usando algunas clases de *Bootstrap* para hacer que la presentación de la página sea un poco más bonita.

####¿cómo se escribían las funciones en las versiones de *ECMAScript* previas a la versión 6?
El truco esta en ubicar el operador `=>` que significa función en *ECMAScript 6*, el código refactorizado para *ECMAScript 5* anterior se puede apreciar a continuación (js-2004-no-ecma-6.js):

```javascript
/**
* Funcion para construir los elementos de la lista
**/
function constructElement(joke, numberJoke, foto){
	var html = '<div class="list-group-item list-group-item-action flex-column align-items-start "><div class="row"><div class="col-md-3"><img class="chuckFoto" src="' + foto +'"></div><div class="col-md-9"><div class="jokeNumber"><small>Chiste número '+ numberJoke+'</small></div><h2>'+joke+'</h2></div></div></div>';
	return html
}
/**
* Funcion para obtener fotos aleatoreas
**/
function fotoName(){
	var number = Math.floor((Math.random() * 5) + 1);
	return 'assets/img/chuck-norris' + number + '.jpg'; 
}

//Uso del pluig solicitando 10 chistes diferentes
$.icndb.getRandomJokes({ 
    number: 10, 
    success: function(response) {
		response.forEach( function(element){ 
			var Name = fotoName();
			//Agregar elementos a la lista
			$("#jokeList").append(constructElement(element.joke, element.id, Name));
		});
	}
});

```
###Resolución del ejercicio con Web Components (chucknow.html)
Debo decir que había escuchado de los Web Components pero nunca antes había usado alguno. Mi impresión: son sencillamente geniales

Además, yo solo he usado *BootStrap* a lo largo de mi vida profesional, así que usar otro framework de front fue muy interesante.

####La vista de la esta página se ve así:
![alt text](https://bytebucket.org/nayko/laboratorio_cliente_web/raw/eed98759ad67abb46bd71489a7283d7dd033383d/screenshots/chucknow.png "Vista chucknow.html")
####Mientras que el código html (chucknow.html) es el siguiente;
```html
<!DOCTYPE html>
<html>
    <head>
        <!--mata tags-->
        <meta charset="UTF-8">
        <meta name="description" content="Hacer un chiste ChuckNorris como en la actualidad">
        <meta name="author" content="Nicolas Pedraza Luengas">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--Titulo de la Pagina-->
        <!--Hoja de estilos y scrpist de bootstrap-->
        <link rel="stylesheet" href="assets/Skeleton-2.0.4/css/normalize.css">
        <link rel="stylesheet" href="assets/Skeleton-2.0.4/css/skeleton.css">
        <!--Estilos propios-->
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
        <!--Caragar Componente-->
        <link rel="import" href="https://raw.githubusercontent.com/erikringsmuth/chuck-norris-joke/master/chuck-norris-joke.html">
        <!--jquery-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </head>

    <body>
        <h1 class="center"> Chuck Norreate de la risa XD con Skeleton</h1>
        <div class="talbaChistes">
            <table class="u-full-width">
                <thead>
                    <tr>
                        <th>Joke Number</th>
                        <th>Joke</th>
                        <th>Foto</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td><chuck-norris-joke></chuck-norris-joke></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><chuck-norris-joke></chuck-norris-joke></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><chuck-norris-joke></chuck-norris-joke></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><chuck-norris-joke></chuck-norris-joke></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><chuck-norris-joke></chuck-norris-joke></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><chuck-norris-joke></chuck-norris-joke></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><chuck-norris-joke></chuck-norris-joke></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><chuck-norris-joke></chuck-norris-joke></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><chuck-norris-joke></chuck-norris-joke></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><chuck-norris-joke></chuck-norris-joke></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--Invocacion de la logica de java script-->
        <script src="assets/js/js-now.js"></script>
    </body>

</html>
```
####Por último, el código de Java Script (js-now.js) es el siguiente:
````javascript
/**
* Funcion para obtener el id del joke de un chuck-norris-joke
**/
getJokeId = (element) =>{
	return element.attr("joke-id");
}

/**
* Funcion para obtener fotos aleatoreas
**/
fotoName = () =>{
	var number = Math.floor((Math.random() * 5) + 1);
	return 'assets/img/chuck-norris' + number + '.jpg'; 
}

//Usar un intevalo que para revisar el id de chiste obtenido
var rellenar = setInterval(() =>{
	var finalizado = true; 
	//recorre todo los ellemntos chuck-norris-joke
	$( "chuck-norris-joke" ).each((index, element ) => {
		//obtener el id del chiste
		var jokeId = getJokeId($(element));
		//si aun no se ha cragado un chiste terminar con el ciclo, de lo conetrario rellenar la informacion
		if (jokeId == undefined || jokeId=="undefined"){
			finalizado = false;
			return false;
		}else{
			tdElelments = $(element).parent().parent().children();
			$(tdElelments[0]).html(jokeId);
			$(tdElelments[2]).html('<img class="chuckFoto" src="'+ fotoName() +'">');
		}
	});
	if(finalizado){
		clearInterval(rellenar);
	}	 
}, 500);
````
##Referencias

* [Foto de Chuck Norris] [Ilustración]. (s.f.). Recuperado de http://pullzone-tiempomx.editorialhondura.netdna-cdn.com/wp-content/uploads/2017/08/chuck.jpg
* [Foto de Chuck Norris] [Ilustración]. (s.f.). Recuperado de https://static.vecer.com/images/slike/2017/07/01/01042394F-1300.jpg
* [Foto de Chuck Norris] [Ilustración]. (s.f.). Recuperado de http://pm1.narvii.com/6520/71288afeff9d159af198037fc948a8972125c8c0_hq.jpg
* [Foto de Chuck Norris] [Ilustración]. (s.f.). Recuperado de https://yt3.ggpht.com/-wR39k8c4aNk/AAAAAAAAAAI/AAAAAAAAAAA/-UQDX07PGa4/s900-c-k-no-mo-rj-c0xffffff/photo.jpg
* Pngimg.com. (s.f.). [Foto de Chuck Norris] [Ilustración]. Recuperado de http://pngimg.com/download/31765/?i=1

