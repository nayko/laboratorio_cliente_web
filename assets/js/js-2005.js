/**
* Funcion para obtener chistes
**/
function getJoke(){
	xmlhttp = new XMLHttpRequest();
	xmlhttp.open('GET', 'http://api.icndb.com/jokes/random/', true);
	xmlhttp.onreadystatechange = function(){
	    var textoChiste = JSON.parse(this.response).value.joke;
	    console.log('chiste recibido: ' + textoChiste);
	    var h1s = document.getElementsByTagName('h1');
	    h1s[0].innerHTML = textoChiste;

	}
	xmlhttp.send();
}
//Se le asiggna al boton un envento click para cambiar de chiste
var boton = document.getElementsByClassName("btn")[0];
boton.addEventListener('click', function() { 
	getJoke(); 
});
//Se llama la funcionpara que se ejecute cuando se carga la pagina
getJoke();