/**
* Funcion para obtener chistes
**/
function getJoke(){
	$.get("http://api.icndb.com/jokes/random", (response) => {
    	var textoChiste = response.value.joke;
    	$('h1').text(textoChiste);
	})
}
//Se le asiggna al boton un envento click para cambiar de chiste
var boton = document.getElementsByClassName("btn")[0];
boton.addEventListener('click', () => { 
	getJoke(); 
});
//Se llama la funcionpara que se ejecute cuando se carga la pagina
getJoke();


