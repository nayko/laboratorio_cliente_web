/**
* Funcion para construir los elementos de la lista
**/
function constructElement(joke, numberJoke, foto){
	var html = '<div class="list-group-item list-group-item-action flex-column align-items-start "><div class="row"><div class="col-md-3"><img class="chuckFoto" src="' + foto +'"></div><div class="col-md-9"><div class="jokeNumber"><small>Chiste número '+ numberJoke+'</small></div><h2>'+joke+'</h2></div></div></div>';
	return html
}
/**
* Funcion para obtener fotos aleatoreas
**/
function fotoName(){
	var number = Math.floor((Math.random() * 5) + 1);
	return 'assets/img/chuck-norris' + number + '.jpg'; 
}

//Uso del pluig solicitando 10 chistes diferentes
$.icndb.getRandomJokes({ 
    number: 10, 
    success: function(response) {
		response.forEach( function(element){ 
			var Name = fotoName();
			//Agregar elementos a la lista
			$("#jokeList").append(constructElement(element.joke, element.id, Name));
		});
	}
});




