/**
* Funcion para obtener el id del joke de un chuck-norris-joke
**/
getJokeId = (element) =>{
	return element.attr("joke-id");
}

/**
* Funcion para obtener fotos aleatoreas
**/
fotoName = () =>{
	var number = Math.floor((Math.random() * 5) + 1);
	return 'assets/img/chuck-norris' + number + '.jpg'; 
}

//Usar un intevalo que para revisar el id de chiste obtenido
var rellenar = setInterval(() =>{
	var finalizado = true; 
	//recorre todo los ellemntos chuck-norris-joke
	$( "chuck-norris-joke" ).each((index, element ) => {
		//obtener el id del chiste
		var jokeId = getJokeId($(element));
		//si aun no se ha cragado un chiste terminar con el ciclo, de lo conetrario rellenar la informacion
		if (jokeId == undefined || jokeId=="undefined"){
			finalizado = false;
			return false;
		}else{
			tdElelments = $(element).parent().parent().children();
			$(tdElelments[0]).html(jokeId);
			$(tdElelments[2]).html('<img class="chuckFoto" src="'+ fotoName() +'">');
		}
	});
	if(finalizado){
		clearInterval(rellenar);
	}	 
}, 500);



